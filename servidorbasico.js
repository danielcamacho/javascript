/*Si enumeramos todos los números naturales por debajo de 10 que son múltiplos de 3 o 5, obtenemos 3, 5, 6 y 9. La suma de estos múltiplos es 23.
Encuentra la suma de todos los múltiplos de 3 o 5 debajo de 1000.*/


ar contador = 0;
var suma = 0;

function resultado() {
    while (contador <= 10) {
        contador++;
        if (!(contador % 3) || !(contador % 5)) {
            suma = suma + contador;
        }
    }
}


//console.log('la suma es '(sum));